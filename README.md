Sean Jones' dotfile ~/ collection
------------------------------

An easy to access location and history of my terminal customization.

## Prompt info

Prompt is inspired by "oh-my-zsh" themes and are customized for my taste.
All other files and scripts are usually hacked together by myself, if not I do my best to comment
where they originated from.

## Supported

| OS | Terminal | Editor | Version Control |
|----|----------|--------|-----------------|
| Mac OS X | Terminal/iTerm | Sublime/Vim | Git |
| Debian | Gnome-terminal | Vim | Git/CVS |
| Ubuntu~ | Gnome-terminal | Vim | Git/CVS |
| CentOS~ | ssh | Vim | Git |

Note:~ - Not yet fully supported

## TODO

* Move away from the oh-my-zsh dependency -- Almost done, Haven't used oh-my-zsh version for sometime
* Remove oh-my-zsh
* Allow git to show if dirty and untracked
* Indication time since last commit in git
* Fix battery level display
