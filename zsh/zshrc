# NeuralSandwich's personalised .zshrc

#############
# Set Paths #
#############

# Path
typeset -U path
path=(~/bin ~/.rvm/bin $path)

# Zsh Function Path
typeset -U fpath
fpath=($ZSH/func "${fpath[@]}")
fpath=($ZSH/func/battery/ "${fpath[@]}")

###############
# Set Options #
###############

# Set Prompt substitution 
setopt prompt_subst

# Show completion on first TAB
setopt menucomplete

# Auto Change Directory
setopt autocd

# Set history options
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS

######################
# Load ZSH functions #
######################

# Load completions for Ruby, Git, etc.
autoload compinit
compinit -i

# Colours / Colours in Prompt
# https://github.com/cyphactor/dotzsh/blob/master/zshenv
autoload -U colors
colors

# Menu styled tab completion
zstyle ':completion:*' menu select

#################
# Include Files #
#################

# Battery Indicator
# My battery plugin from oh-my-zsh
# http://github.com/NeuralSandwich/oh-my-zsh.git
source "$ZSH/func/battery/battery.zsh"

# Git Prompt
# https://github.com/olivierverdier/zsh-git-prompt
source "$ZSH/func/git-prompt/zshrc.sh"

# Weclome
source "$ZSH/func/welcome/welcome.zsh"

# SSH Agent
source "$DOTFILES/ssh-agent/ssh-agent.sh"

# ssh_client
source "$ZSH/func/ssh_client/ssh_client.zsh"

# extract
source "$ZSH/func/extract/extract.zsh"

# mkarchive
source "$ZSH/func/mkarchive/mkarchive.zsh"

# key bindings
source "$ZSH/zkeybindings"

# alias
source "$ZSH/zsh_aliases"

# Load RVM into a shell session *as a function*
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

###############
# Set Prompts #
###############

# Prompt
PROMPT='
%{$fg[magenta]%}%n%{$reset_color%}@%{$fg[yellow]%}%m%{$fg[red]%}%{$ssh_client%}%{$reset_color%}:%{$fg_bold[green]%}${PWD/#$HOME/~}%{$reset_color%}$(git_super_status)
$ '

# Right Prompt
local return_status="%{$fg[red]%}%(?..✘)%{$reset_color%}"
RPROMPT='${return_status}[%t]$(battery_time_remaining) $(battery_pct_prompt)%{$reset_color%}'
